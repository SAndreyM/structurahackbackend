from pathlib import Path

from flask import Flask
from flask_migrate import Migrate, upgrade, migrate
from flask_cors import CORS

from src.app.model import db
from src.app.model import visitCard, users


def build_app() -> Flask:
    app = Flask(__name__)
    CORS(app)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgre@localhost/visit_cards'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db.init_app(app)
    mig = Migrate(app, db)
    with app.app_context():
        migrate('migrations')
        upgrade('migrations')

    for module in Path().glob('src/app/rest/**/*.py'):
        module = __import__(str(module)[:-3].replace('/', '.'), fromlist=['bp'])
        if hasattr(module, 'bp'):
            app.register_blueprint(module.bp)

    return app


app = build_app()


@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  response.headers.add('Access-Control-Allow-Credentials', 'true')
  return response


@app.route('/')
def hello_world():  # put application's code here
    return 'Hello World!'


if __name__ == '__main__':
    app.run()
