from src.app.model import db
from sqlalchemy import Table, Column, MetaData, Integer, ForeignKey, BigInteger, String

metadata = MetaData()


class Users(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String, nullable=False)
    password = db.Column(db.String, nullable=False)


ATokens = Table('a_tokens', db.metadata,
            Column('user_id', Integer(), ForeignKey("users.id"), nullable=False),
            Column('token', String(), nullable=False, index=True),
            Column('date', BigInteger(), nullable=False)
            )
