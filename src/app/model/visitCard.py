from src.app.model import db
from sqlalchemy import Table, Column, MetaData, Integer, String, ForeignKey

metadata = MetaData()


class VisitCards(db.Model):
    __tablename__ = "visit_cards"
    cardId = db.Column('id', db.Integer, primary_key=True)
    ownerId = db.Column('owner_id', Integer(), ForeignKey("users.id"), nullable=False)
    name = db.Column('name', String(), nullable=False)
    surname = db.Column('surname', String(), nullable=False)
    patronymic = db.Column('patronymic', String(), nullable=True)
    email = db.Column('email', String(), nullable=True)
    work = db.Column('work', String(), nullable=True)
    rang = db.Column('rang', String(), nullable=True)
    points = db.Column('points', Integer(), nullable=False)
    hash = db.Column('hash', String(), nullable=False, index=True)

    def to_json(self) -> dict:
        return dict(
            cardId=self.cardId,
            id=self.ownerId,
            name=self.name,
            surname=self.surname,
            patronymic=self.patronymic,
            email=self.email,
            work=self.work,
            rang=self.rang,
            hash=self.hash,
        )


Tags = Table('card_tags', db.metadata,
             Column('card_id', Integer(), ForeignKey("visit_cards.id"), nullable=False),
             Column('tag', Integer(), nullable=False)
             )

Socials = Table('card_socials', db.metadata,
                Column('card_id', Integer(), ForeignKey("visit_cards.id"), nullable=False),
                Column('social_net_id', Integer(), nullable=False),
                Column('social_link', String(), nullable=False)
                )

AddedCards = Table('added_cards', db.metadata,
                   Column('id', Integer(), primary_key=True, nullable=False),
                   Column('card_id', Integer(), ForeignKey("visit_cards.id"), nullable=False),
                   Column('owner_id', Integer(), ForeignKey("users.id"), nullable=False),
                   Column('referer_id', Integer(), ForeignKey("visit_cards.id"), nullable=True),
                   Column('description', String(), nullable=False),
                   Column('points', Integer(), nullable=False),
                   Column('hash', String(), nullable=False, index=True),
                   )
