from hashlib import sha256
from random import randint

from flask import Blueprint, request
from src.app.model import db, users, visitCard

bp = Blueprint('addInfo', __name__, url_prefix='/add_info')


@bp.route('/get_tags', methods=['GET'])
def createVisitCard():
    return {"code": 200,
            "response": {
                "status": "ok",
                "data": [
                    {"id": 0, "label": "Гендиректор"},
                    {"id": 1, "label": "Самозанятый"},
                    {"id": 2, "label": "Инвестор"},
                    {"id": 3, "label": "Студент"},
                    {"id": 4, "label": "Врач"},
                ]
            }
            }


@bp.route('/social_nets', methods=['GET'])
def createAddedCard():
    return {"code": 200,
            "response": {
                "status": "ok",
                "data": [
                    {"id": 0, "label": "Telegram"},
                ]
            }
            }
