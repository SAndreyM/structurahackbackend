from datetime import datetime
import random
import string
import time
from hashlib import sha256
from typing import Dict

from sqlalchemy import and_
import sqlalchemy
from flask import Blueprint, request, g, send_file

from src.app.model import db, users, visitCard

bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/', methods=['GET', 'POST'])
def authorize():
    if request.method == 'POST':
        form = request.json
        login = form.get("login")
        passw = form.get("pass")

        if (login is None) or (passw is None):
            return {"code": 403, "response": {"status": "error", "data": "Не указан логин или пароль"}}

        passw = sha256(passw.encode()).hexdigest()

        user = db.session.query(users.Users).filter(
            and_(users.Users.login == login, users.Users.password == passw)
        ).first()

        if user is None:
            return {"code": 403, "response": {"status": "error", "data": "Неверный логин или пароль"}}

        userIdCol = sqlalchemy.sql.column('user_id')

        stmt = users.ATokens.delete().where(userIdCol == user.id)
        with db.engine.connect() as conn:
            conn.execute(stmt)
            conn.commit()

        now = datetime.now().replace(microsecond=0)
        now = int(datetime.timestamp(now))

        tokenStr = f"{now}.{user.login}.{user.password}.{random.randint(0, 100)}".encode()
        token = sha256(tokenStr).hexdigest()

        stmt = users.ATokens.insert().values(user_id=user.id, token=f"{token}", date=now)
        with db.engine.connect() as conn:
            conn.execute(stmt)
            conn.commit()

        vizits = db.session.query(visitCard.VisitCards).filter(visitCard.VisitCards.ownerId == user.id)
        if vizits.count() == 0:
            return {
                "code": 200,
                "response": {
                    "status": "ok",
                    "data": dict(id=user.id, login=user.login, vizits=[], token=token)
                }
            }

        vizits = [x.to_json() for x in vizits]
        fullVizits = []
        for vizitC in vizits:
            cardId = sqlalchemy.sql.column('card_id')

            stmt = visitCard.Socials.select().where(cardId == vizitC["cardId"])
            socialNets = None
            with db.engine.connect() as conn:
                socialNets = conn.execute(stmt)
            Socials = [{"id": socialNet.social_net_id, "link": socialNet.social_link} for socialNet in socialNets]

            stmt = visitCard.Tags.select().where(cardId == vizitC["cardId"])
            tags = None
            with db.engine.connect() as conn:
                tags = conn.execute(stmt)
            Tags = [tag.tag for tag in tags]
            fullVizits.append(vizitC | dict(socialNets=Socials) | dict(tags=Tags))

        return {
            "code": 200,
            "response": {
                "status": "ok",
                "data": dict(
                    id=user.id,
                    login=user.login,
                    vizits=fullVizits,
                    token=token
                )
            }
        }
    else:
        token = request.values.get("token")
        if token is None:
            return {
                "code": 403,
                "response": {
                    "status": "error",
                    "data": "Токен не был передан"
                }
            }

        tokenCol = sqlalchemy.sql.column('token')

        stmt = users.ATokens.select().where(tokenCol == token)
        result = None
        with db.engine.connect() as conn:
            result = conn.execute(stmt)

        tokenRecord = result.first()
        now = datetime.now().replace(microsecond=0)
        now = int(datetime.timestamp(now)) - 10

        if (tokenRecord is None) or (now >= tokenRecord.date):
            return {
                "code": 200,
                "response": {
                    "status": "error",
                    "data": "Токен невалиден"
                }
            }

        user = db.session.query(users.Users).filter(users.Users.id == tokenRecord.user_id).first()

        vizits = db.session.query(visitCard.VisitCards).filter(visitCard.VisitCards.ownerId == user.id)
        if vizits.count() == 0:
            return {
                "code": 200,
                "response": {
                    "status": "ok",
                    "data": dict(id=user.id, login=user.login, vizits=[])
                }
            }

        vizits = [x.to_json() for x in vizits]
        return {
            "code": 200, "response": {
                "status": "ok",
                "data": dict(id=user.id, login=user.login, vizits=vizits)
            }
        }
