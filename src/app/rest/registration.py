from hashlib import sha256
from flask import Blueprint, request
from src.app.model import db, users

bp = Blueprint('registration', __name__, url_prefix='/registration')


@bp.route('/', methods=['POST'])
def authorize():
    form = request.json
    login = form.get("login")
    passw = form.get("pass")

    if (login is None) or (passw is None):
        return {"code": 403, "response": {"status": "error", "data": "Не указан логин или пароль"}}

    user = db.session.query(users.Users).filter(users.Users.login == login).first()
    if user is not None:
        return {"code": 200, "response": {"status": "error", "data": "Пользователь с данным логином уже существует"}}

    passw = sha256(passw.encode()).hexdigest()
    user = users.Users(login=login, password=passw)
    db.session.add(user)
    db.session.commit()
    return {"code": 200, "response": {"status": "ok", "data": ""}}
