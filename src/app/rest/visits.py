from hashlib import sha256
from random import randint

import sqlalchemy
from sqlalchemy import not_, desc
from flask import Blueprint, request
from src.app.model import db, users, visitCard

bp = Blueprint('visits', __name__, url_prefix='/visits')


@bp.route('/create_visit_card', methods=['POST'])
def createVisitCard():
    form = request.json
    ownerId = form.get("ownerId")
    name = form.get("name")
    surname = form.get("surname")
    if (ownerId is None) or (name is None) or (surname is None):
        return {
            "code": 403,
            "response": {
                "status": "error",
                "data": "Не указана фамилия, имя, или идентификатор владельца"
            }
        }
    patronymic = form.get("patronymic")
    email = form.get("email")
    work = form.get("work")
    rang = form.get("rang")
    tags = form.get("tags")
    socialNets = form.get("sNets")

    hashStr = f"{ownerId}.{name}.{surname}.{patronymic}-{email}-{work}+{rang}+" \
              f"{randint(0, 1000)}".encode()
    hashStr = sha256(hashStr).hexdigest()

    visitC = visitCard.VisitCards(
        ownerId=ownerId,
        name=name,
        surname=surname,
        patronymic=patronymic,
        email=email,
        work=work,
        rang=rang,
        points=0,
        hash=hashStr
    )

    db.session.add(visitC)
    db.session.commit()
    for i in tags:
        stmt = visitCard.Tags.insert().values(card_id=visitC.cardId, tag=f"{i}")
        with db.engine.connect() as conn:
            conn.execute(stmt)
            conn.commit()

    for i in socialNets:
        stmt = visitCard.Socials.insert().values(
            card_id=visitC.cardId,
            social_net_id=i['net_id'],
            social_link=i["link"]
        )
        with db.engine.connect() as conn:
            conn.execute(stmt)
            conn.commit()
    return {"code": 200, "response": {"status": "ok", "data": ""}}


@bp.route('/create_added_cards', methods=['POST'])
def createAddedCard():
    form = request.json
    rootCard = form["root_card"]
    refererCard = form["refererCard"]
    owner = form["owner_id"]
    hashStr = f"{rootCard}.{refererCard}.{owner}.{randint(0, 1000)}".encode()
    hashStr = sha256(hashStr).hexdigest()

    stmt = visitCard.AddedCards.insert().values(
        card_id=rootCard,
        owner_id=owner,
        referer_id=refererCard,
        description="",
        points=0,
        hash=hashStr
    )
    with db.engine.connect() as conn:
        conn.execute(stmt)
        conn.commit()
    return {"code": 200, "response": {"status": "ok", "data": ""}}

@bp.route('/update_descr_added_card', methods=['POST'])
def updateDescrAddedCard():
    form = request.json
    cardId = form.get("cardId")
    if cardId is None:
        return {"code": 403, "response": {"status": "error", "data": "Не был передан идентификатор визитки"}}
    desc = form.get("desc")

    stmt = visitCard.AddedCards.update().where(id=cardId).values(description=desc)
    with db.engine.connect() as conn:
        conn.execute(stmt)
        conn.commit()
    return {"code": 200, "response": {"status": "ok", "data": ""}}


@bp.route('/get_card', methods=['POST'])
def getCard():
    form = request.json
    hash = form.get("hash")
    type = form.get("type")
    if (hash is None) or (type is None):
        return {"code": 403, "response": {"status": "error", "data": "Такой визитки не существует"}}
    cardId = sqlalchemy.sql.column('card_id')
    if type:
        visitC = db.session.query(visitCard.VisitCards).filter(visitCard.VisitCards.hash == hash).first()
        if visitC is None:
            return {"code": 403, "response": {"status": "error", "data": "Такой визитки не существует"}}
        stmt = visitCard.Socials.select().where(cardId == visitC.cardId)
        socialNets = None
        with db.engine.connect() as conn:
            socialNets = conn.execute(stmt)
        Socials = [{"id": socialNet.social_net_id, "link": socialNet.social_link} for socialNet in socialNets]

        stmt = visitCard.Tags.select().where(cardId == visitC.cardId)
        tags = None
        with db.engine.connect() as conn:
            tags = conn.execute(stmt)
        Tags = [tag.tag for tag in tags]
        return {"code": 200, "response": {"status": "ok", "data": visitC.to_json()|
                                                                    dict(tags=Tags)|
                                                                    dict(socialNets=Socials)}}
    else:
        visitC = None
        stmt = visitCard.AddedCards.select().filter(hash == hash)
        with db.engine.connect() as conn:
            visitC = conn.execute(stmt).first()
        if visitC is None:
            return {"code": 403, "response": {"status": "error", "data": "Такой визитки не существует"}}
        print(visitC.card_id)
        visitC = db.session.query(visitCard.VisitCards).filter(visitCard.VisitCards.cardId == visitC.card_id).first()
        if visitC is None:
            return {"code": 403, "response": {"status": "error", "data": "Такой визитки не существует"}}
        stmt = visitCard.Socials.select().where(cardId == visitC.cardId)
        socialNets = None
        with db.engine.connect() as conn:
            socialNets = conn.execute(stmt)
        Socials = [{"id": socialNet.social_net_id, "link": socialNet.social_link} for socialNet in socialNets]

        stmt = visitCard.Tags.select().where(cardId == visitC.cardId)
        tags = None
        with db.engine.connect() as conn:
            tags = conn.execute(stmt)
        Tags = [tag.tag for tag in tags]
        visitC.hash = hash
        return {"code": 200, "response": {"status": "ok", "data": visitC.to_json() |
                                                                  dict(tags=Tags) |
                                                                  dict(socialNets=Socials)}}

@bp.route('/update_visit_card', methods=['POST'])
def updateVisitCard():
    form = request.json
    cardId = form.get("cardId")
    name = form.get("name")
    surname = form.get("surname")
    if (cardId is None) or (name is None) or (surname is None):
        return {
            "code": 403,
            "response": {
                "status": "error",
                "data": "Фамилия и имя не могут быть пустыми"
            }
        }
    cardIdCol = sqlalchemy.sql.column('card_id')

    for x in list(form.keys()):
        match x:
            case "cardId":
                pass
            case "points":
                pass
            case "tags":
                tags = None
                stmt = visitCard.Tags.select().filter(cardIdCol == cardId)
                with db.engine.connect() as conn:
                    tags = conn.execute(stmt)
                for _ in tags:
                    stmt = visitCard.Tags.delete().where(cardIdCol == cardId)
                    with db.engine.connect() as conn:
                        conn.execute(stmt)
                        conn.commit()
                for i in form.get(x):
                    stmt = visitCard.Tags.insert().values(card_id=cardId, tag=i)
                    with db.engine.connect() as conn:
                        conn.execute(stmt)
                        conn.commit()
            case "sNets":
                sNets = None
                stmt = visitCard.Socials.select().where(cardIdCol == cardId)
                with db.engine.connect() as conn:
                    sNets = conn.execute(stmt)
                for i in sNets:
                    stmt = visitCard.Socials.delete().where(cardIdCol == cardId)
                    with db.engine.connect() as conn:
                        conn.execute(stmt)
                        conn.commit()
                for i in form.get(x):
                    stmt = visitCard.Socials.insert().values(
                        card_id=cardId,
                        social_net_id=i["social_net_id"],
                        social_link=i["social_link"]
                    )
                    with db.engine.connect() as conn:
                        conn.execute(stmt)
                        conn.commit()
            case _:
                visits = db.session.query(visitCard.VisitCards).filter(visitCard.VisitCards.cardId == cardId).first()
                setattr(visits, x, form.get(x))
                db.session.add(visits)
                db.session.commit()

    return {
            "code": 200,
            "response": {
                "status": "ok",
                "data": ""
            }
        }
